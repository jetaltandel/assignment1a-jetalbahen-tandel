﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="Assignment1a.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Booking</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>Hotel Booking</p>
            <label>First Name</label>
            <asp:TextBox runat="server" ID="clientName"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Name" ControlToValidate="clientName" ID="validatorName"></asp:RequiredFieldValidator>
            <br /><br/>
            <label>Phone No:</label>
            <asp:TextBox runat="server" ID="clientPhone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <br />
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="This is an invalid phone number"></asp:CompareValidator>
            <br />
            <label>Email ID</label>
            <asp:TextBox runat="server" ID="clientEmail" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br /><br/>
            <label>Adult</label>
             <asp:DropDownList runat="server" ID="AdultNo">
              <asp:ListItem Value="1" Text="one"></asp:ListItem>
                <asp:ListItem Value="2" Text="two"></asp:ListItem>
                <asp:ListItem Value="3" Text="three"></asp:ListItem>
            </asp:DropDownList>
            <br /><br/>
            <label>Room Type:</label>
             <asp:RadioButton runat="server" Text="Single" GroupName="room"/>
            <asp:RadioButton runat="server" Text="Queen" GroupName="room"/>
            <br /><br/>
            <div id="Services" runat="server">
            <label>Services:<label/>
            <asp:CheckBox runat="server" ID="Service1" Text="Wi-Fi" />
            <asp:CheckBox runat="server" ID="Service2" Text="Breakfast" />
            <asp:CheckBox runat="server" ID="Service3" Text="Parking" />
            </div>
            <br />
            <div>
             <label>Chech In</label>
             <asp:Calendar runat="server" />
             <label>Chech Out</label>
             <asp:Calendar runat="server" />
            </div>
            <br/>
            <div>
            <label>CheckOutTime</label>
            <asp:TextBox runat="server" ID="time"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="time" Type="Integer" MinimumValue="8" MaximumValue="12" ErrorMessage="Enter a valid time of chechout (between 8am to 12pm)"></asp:RangeValidator>
            <br/>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter CheckOutTime" ControlToValidate="time" ID="RequiredFieldValidator3"></asp:RequiredFieldValidator>

            </div>
            <br/>
            <asp:ValidationSummary ID="ValidationSummary1"  runat="server"  HeaderText="Following error occurs....." ShowMessageBox="false" 
            DisplayMode="BulletList" ShowSummary="true"   BackColor="Snow" Width="350" ForeColor="blue" Font-Size="medium" Font-Italic="true" />
          <asp:Button runat="server" ID="Button" Text="Submit"/>
           </div>
    </form>
</body>
</html>
